// Додати нові продукти (Відео для відео хостингу та страви для ресторану)
// Відео має бути 3 зовнішнім посиланням та 3 відео завантажені до проекту

// Тут реалізація вашого коду.

import { dateNow, generationId } from "./functions.js";

class VideoElementCRM {
  constructor(
    productName = "",
    like = 0,
    url = "/img/error.png",
    description = "",
    poster = "/img/poster.png",
    keywords = [],
    dateNow = () => {},
    id = () => {}
  ) {
    this.id = id();
    this.date = dateNow();
    this.productName = productName;
    this.like = like;
    this.url = url;
    this.description = description;
    this.poster = poster;
    this.keywords = keywords.split(",");
    this.status = false;
  }
}

export { VideoElementCRM };

class RestElementCRM {
  constructor(
    productName = "",
    productWeiht = "",
    ingredients = "",
    price = 0,
    productImageUrl = "/img/error.png",
    keywords = [],
    Weiht = 0,
    like = 0,
    dateNow = () => {},
    id = () => {}
  ) {
    this.id = id();
    this.data = dateNow();
    this.productName = productName;
    this.productWeiht = productWeiht;
    this.ingredients = ingredients;
    this.price = price;
    this.productImageUrl = productImageUrl;
    this.keywords = keywords.split(",");
    (this.Weiht = Weiht),
      (this.stopList = false),
      (this.ageRestrictions = false),
      (this.like = like);
  }
}

export { RestElementCRM };

// -------------------------------------------------------------------------

export function saveDataHW() {
  try {
    const [isCategory] = document.querySelector("select").selectedOptions;
    const [...inputs] = document.querySelectorAll("form input");
    if (isCategory.value === "Відео хостинг") {
      const obj = {
        productName: "string",
        url: "string",
        poster: "string",
        description: "string",
        keywords: "string array",
      };

      inputs.forEach((e) => {
        obj[e.dataset.type] = e.value;
        e.value = "";
      });

      const video = JSON.parse(localStorage.video);
      video.push(
        new VideoElementCRM(
          obj.productName,
          undefined,
          obj.url,
          obj.description,
          obj.poster,
          obj.keywords,
          dateNow,
          generationId
        )
      );

      localStorage.video = JSON.stringify(video);

      // ---------------------------------------------
    } else if (isCategory.value === "Рестаран") {
      const obj = {
        productName: "string",
        productWeiht: "string",
        ingredients: "string",
        price: "number",
        productImageUrl: "string",
        keywords: "string array",
        Weiht: "number",
      };

      inputs.forEach((e) => {
        obj[e.dataset.type] = e.value;
        e.value = "";
      });

      const rest = JSON.parse(localStorage.restorationBD);
      rest.push(
        new RestElementCRM(
          obj.productName,

          obj.productWeiht,
          obj.ingredients,
          obj.price,
          obj.productImageUrl,
          obj.keywords,
          obj.Weiht,
          undefined,
          dateNow,
          generationId
        )
      );

      localStorage.restorationBD = JSON.stringify(rest);
    }
  } catch (e) {
    console.error(e);
  }
}
