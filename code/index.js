import { getLogin, getPassword, modalClose, modalSave } from "./var.js";
import {
  changeInputEvent,
  userLoginEvent,
  showModalEvent,
  hideModalEvent,
  saveData,
  exportDataEvent,
} from "./events.js";
// import { createHTMLElement, categorySelect } from "./functions.js";
import { req, categorySelect } from "./functions.js";

// мой код ---------------------------------------------------------------------------
import { saveDataHW } from "./code.js";

// -----------------------------------------------------------------------------------

const EXPORT = document.querySelector("#export");
const REQ = document.getElementById("req");

if (
  !sessionStorage.isLogin &&
  !document.location.pathname.includes("/authorization")
) {
  document.location = "/authorization";
}
//authorization
try {
  document
    .querySelector(".window form")
    .addEventListener("change", changeInputEvent);

  document.getElementById("disabled").addEventListener("click", userLoginEvent);
} catch (error) {
  // console.error(error)
}

//main page
try {
  document.querySelector(".add").addEventListener("click", showModalEvent);

  const modalWindow = document.querySelector(".container-modal .modal");

  // const modalSave = createHTMLElement("button", "btn-save", "Зберегти", [
  //   { type: "button" },
  //   { "data-type": "button" },
  // ]);
  // const modalClose = createHTMLElement("button", "btn-close", "Скасувати", [
  //   { type: "button" },
  //   { "data-type": "button" },
  // ]);

  modalWindow.insertAdjacentHTML(
    "beforeend",
    `
    <h2>Додайте новий продукт до БД</h2>
    <div class="catigoty"></div>
    <div class="modal__body"></div>
    <div class="modal__control"></div>
    `
  );

  document.querySelector(".modal__control").append(modalSave, modalClose);
  categorySelect();

  modalClose.addEventListener("click", hideModalEvent);

  modalSave.addEventListener("click", saveData);

  /*
    Ваші події 
    */
  // мой код ---------------------------------------------------------------------------
  modalSave.addEventListener("click", saveDataHW);

  // -----------------------------------------------------------------------------------
} catch (e) {
  // console.error(e);
}

try {
  EXPORT.addEventListener("click", exportDataEvent);
  REQ.addEventListener("click", () => {
    req("fetch", "https://jsonplaceholder.typicode.com/comments");
  });
} catch (e) {}

console.log(getLogin, getPassword);

if (!localStorage.store) {
  localStorage.store = JSON.stringify([]);
}
if (!localStorage.video) {
  localStorage.video = JSON.stringify([]);
}
if (!localStorage.restorationBD) {
  localStorage.restorationBD = JSON.stringify([]);
}
